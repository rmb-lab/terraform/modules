variable "application" {
  type = "string"
}

variable "custom_consul_policy" {
  type = "string"
  default = ""
}

variable "consul_token_type" {
  type = "string"
  default = "client"
}

variable "custom_vault_policy" {
  type = "string"
  default = ""
}

variable "kubernetes_path" { 
  type = "string"
  default = "kubernetes"
}

variable "kubernetes_namespace" {
  type = "string"
  default = ""
}

variable "kubernetes_service_account" {
  type = "string"
  default = "default"
}

variable "kubernetes_token_ttl" {
  type = "string"
  default = "3600"
}

variable "sandwich_cloud_project" {
  type = "string"
  default = ""
}

variable "sandwich_cloud_service_account" {
  type = "string"
  default = "default"
}
