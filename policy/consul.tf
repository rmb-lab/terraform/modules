resource "vault_consul_role" "role" {
  name = "${var.application}"
  token_type = "${var.consul_token_type}"
  policy = <<EOT
# Node
node "" {
  policy = "read"
}
node "${var.application}" {
  policy = "write"
}

# Service
service "" {
  policy = "read"
}
service "${var.application}" {
  policy = "write"
}

# Session
session "" {
  policy = "read"
}
session "${var.application}" {
  policy = "write"
}

# Config KVs
key "/config/${var.application}" {
  policy = "read"
}
key "/config/${var.application}" {
  policy = "list"
}

# Custom Policies
${var.custom_consul_policy}
EOT
}
