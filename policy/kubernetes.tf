resource "vault_generic_secret" "kubernetes-role" {
  count = "${var.kubernetes_namespace != "" ? 1 : 0}"
  disable_read = true
  path = "${var.kubernetes_path}/role/${var.application}"
  data_json = <<EOT
{
  "bound_service_account_names": "${var.kubernetes_service_account}",
  "bound_service_account_namespaces": "${var.kubernetes_namespace}",
  "policies": ["default", "${var.application}"],
  "ttl": "${var.kubernetes_token_ttl}",
  "period": "${var.kubernetes_token_ttl}"
}
EOT
}
