resource "kubernetes_service_account" "user_sa" {
  metadata {
    name = "user"
    namespace = "${lookup(kubernetes_namespace.namespace.metadata[0], "name")}"
  }
}

resource "kubernetes_role_binding" "user_sa" {
  metadata {
    name = "user"
    namespace = "${lookup(kubernetes_namespace.namespace.metadata[0], "name")}"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind = "ClusterRole"
    name = "edit"
  }
  subject {
    kind = "ServiceAccount"
    name = "${lookup(kubernetes_service_account.user_sa.metadata[0], "name")}"
    namespace = "${lookup(kubernetes_namespace.namespace.metadata[0], "name")}"
  }
}
