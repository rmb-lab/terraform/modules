variable "name" {
  type = "string"
}

variable "limits" {
  type = "map"
  default = {
    cpu = "200m"
    memory = "256Mi"
  }
}

variable "requests" {
  type = "map"
  default = {
    cpu = "100m"
    memory = "128Mi"
  }
}

variable "quota" {
  type = "map"
  default = {
    "requests.cpu" = "2"
    "requests.memory" = "5Gi"
    "limits.cpu" = "4"
    "limits.memory" = "8Gi"
  }
}

output "namespace" {
  value = "${lookup(kubernetes_namespace.namespace.metadata[0], "name")}"
}
