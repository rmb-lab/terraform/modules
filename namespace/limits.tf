resource "kubernetes_limit_range" "limits" {
  metadata {
    name = "limits"
    namespace = "${lookup(kubernetes_namespace.namespace.metadata[0], "name")}"
  }
  spec {
    limit {
      type = "Container"
      default = "${var.limits}"
      default_request = "${var.requests}"
    }
  }
}
