resource "kubernetes_resource_quota" "quota" {
  metadata {
    name = "quota"
    namespace = "${lookup(kubernetes_namespace.namespace.metadata[0], "name")}"
  }
  spec {
    hard = "${var.quota}"
  }
}
