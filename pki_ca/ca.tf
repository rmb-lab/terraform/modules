resource "vault_mount" "pki" {
  path = "${var.ca_mount_path}"
  type = "pki"
  description = "PKI Backend for ${var.description}"
}

resource "vault_pki_config_ca" "ca" {
  backend = "${vault_mount.pki.path}"
  cert = "${var.ca_cert}"
  key = "${var.ca_key}"
}

resource "vault_pki_config_urls" "ca" {
  backend = "${vault_mount.pki.path}"
  issuing_certificates = [
    "${var.vault_address}/v1/${vault_mount.pki.path}/ca"
  ],
  crl_distribution_points = [
    "${var.vault_address}/v1/${vault_mount.pki.path}/crl"
  ],
}
