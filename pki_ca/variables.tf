variable "vault_address" {
  type = "string"
}

variable "ca_mount_path" {
  type = "string"
}

variable "ca_cert" {
  type = "string"
}

variable "ca_key" {
  type = "string"
}

variable "description" {
  type = "string"
}

output "ca_mount_path" {
  value = "${vault_mount.pki.path}"
}
